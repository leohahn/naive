Naive
=====

Para executar: naive --help / naive -h / naive <data path> <indices path>

Para rodar, é necessário ter Erlang instalado. Para recompilar ele, instalar também elixir, e
rodar o seguinte comando no diretório raíz:

_mix escript.build_
