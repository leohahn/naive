defmodule Naive.Learning do

  @moduledoc """
  This module implements the Naive Bayes algorithm. It requires the
  data necessary for training and learning.
  """

  import Naive.FormattedData, only: [iteration_class_data: 1,
                                     format_results: 1]
  @max_iteration 10
  
  @doc """
  Create the necessary parameters for the naive bayes algorithm, mean and 
  variance.
  """
  def setup(processed_data) do
    processed_data
    |> start_training(%{}, 0)
    |> format_results
  end

  def start_training(_, iter_data, @max_iteration), do: iter_data
  def start_training(processed_data, iter_data ,iter) do
    # Tuple that selects current testing data. The rest is for training.
    {lower, upper} = {0 + 500 * iter, 499 + 500 * iter} 
    indexed_data  = Enum.with_index(processed_data)
    training_data = Enum.filter indexed_data, fn {_str, index} ->
      index < lower or index > upper
    end
    testing_data  = Enum.filter indexed_data, fn {_str, index} ->
      index >= lower and index <= upper
    end
    classes_info = calculate_mean_and_variance(training_data,
                                               %Naive.Classes{}, 0)
   
    result = Map.put_new(iter_data,
                         iter,
                         start_testing(testing_data, classes_info))
    
    start_training(processed_data, result, iter + 1)
  end

  @doc """
  Given the `training data`, returns a `Naive.Classes` struct, with the
  mean and variance of each class `[0..9]`
  """
  def calculate_mean_and_variance(_, classes, 10), do: classes
  def calculate_mean_and_variance(training_data, classes, class) do
    raw_class_data = Enum.filter training_data, fn {str, _index} ->
      String.ends_with?(str, Integer.to_string(class))
    end
    class_data = Enum.map raw_class_data, fn {str, _index} ->
      str
      |> String.split(",")
      |> List.delete_at(-1)
    end
    class_mean = calculate_mean(class_data)
    class_variance = calculate_variance(class_data, class_mean)
    prob_class = probability_of_class(training_data, class)
    
    updated_classes = cond do
      class == 0 -> %{classes | c0: {class_mean, class_variance, prob_class}}
      class == 1 -> %{classes | c1: {class_mean, class_variance, prob_class}}
      class == 2 -> %{classes | c2: {class_mean, class_variance, prob_class}}
      class == 3 -> %{classes | c3: {class_mean, class_variance, prob_class}}
      class == 4 -> %{classes | c4: {class_mean, class_variance, prob_class}}
      class == 5 -> %{classes | c5: {class_mean, class_variance, prob_class}}
      class == 6 -> %{classes | c6: {class_mean, class_variance, prob_class}}
      class == 7 -> %{classes | c7: {class_mean, class_variance, prob_class}}
      class == 8 -> %{classes | c8: {class_mean, class_variance, prob_class}}
      class == 9 -> %{classes | c9: {class_mean, class_variance, prob_class}}
    end

    calculate_mean_and_variance(training_data, updated_classes, class + 1)
  end

  def probability_of_class(training_data, class) do
    size = Enum.count(training_data)

    class_size = training_data
    |> Enum.map(fn {str, _} -> str end)
    |> Enum.filter(&(String.ends_with?(&1, Integer.to_string(class))))
    |> Enum.count

    class_size / size
  end
  
  def calculate_variance(class_data, class_mean) do
    size = Enum.count(class_data)

    class_data
    |> Enum.map(&(minus_mean_squared(&1, class_mean)))
    |> Enum.reduce(fn list1, list2 ->
      sum_lists(list1, list2)
    end)
    |> Enum.map(fn e -> e/size end)
  end

  defp minus_mean_squared(list, mean_list) do
    list
    |> Enum.zip(mean_list)
    |> Enum.map fn {val, med} ->
      #IO.inspect {val, med}
      {f, _} = Float.parse(val)
      :math.pow(f - med, 2.0)
    end
  end
  
  defp calculate_mean(class_data) do
    size = Enum.count(class_data)
    #IO.inspect class_data
    class_data
    |> Enum.reduce(fn list1, list2 ->
      sum_lists(list1, list2)
    end)
    |> Enum.map(fn e -> e/size end)
  end

  defp sum_lists(list1, list2) do
    List.flatten(list1)
    |> Enum.zip(List.flatten(list2))
    |> Enum.map fn
      {e1, e2} when (is_binary(e1) and is_binary(e2)) ->
        {f1, _} = Float.parse(e1)
        {f2, _} = Float.parse(e2)
        f1 + f2
      {e1, e2} when (is_binary(e1) and is_float(e2)) ->
        {f1, _} = Float.parse(e1)
        f1 + e2
      {e1, e2} when is_float(e1) and is_float(e2) ->
        e1 + e2 
    end
  end

  def start_testing(testing_data, classes_info) do
    testing_data
    |> Enum.map(&(make_prediction(&1, classes_info)))
    |> iteration_class_data
  end

  def make_prediction(str_vector, classes_info) do
    {str, _index} = str_vector
    class = String.to_integer(String.last(str))

    str
    |> String.split(",")
    |> Enum.map(fn e ->
      {f, _} = Float.parse(e)
      f
    end)
    |> List.delete_at(-1)
    |> prob_given_class(classes_info, %{}, 0)
    |> (fn {key, val} -> {key, val, class} end).()
  end

  # p(v|class)
  def prob_given_class(_vector, classes_info, prob, 10) do
    prob
    |> Map.to_list
    |> Enum.max_by fn {key, value} ->
      value
    end
  end
  
  def prob_given_class(vector, classes_info, prob, iter) do
    cond do
      iter == 0 ->
        {mean, variance, prob_class} = classes_info.c0
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c0, p), iter + 1)
      iter == 1 ->
        {mean, variance, prob_class} = classes_info.c1
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c1, p), iter + 1)
      iter == 2 ->
        {mean, variance, prob_class} = classes_info.c2
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c2, p), iter + 1)
      iter == 3 ->
        {mean, variance, prob_class} = classes_info.c3
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c3, p), iter + 1)
      iter == 4 ->
        {mean, variance, prob_class} = classes_info.c4
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c4, p), iter + 1)
      iter == 5 ->
        {mean, variance, prob_class} = classes_info.c5
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c5, p), iter + 1)
      iter == 6 ->
        {mean, variance, prob_class} = classes_info.c6
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c6, p), iter + 1)
      iter == 7 ->
        {mean, variance, prob_class} = classes_info.c7
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c7, p), iter + 1)
      iter == 8 ->
        {mean, variance, prob_class} = classes_info.c8
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c8, p), iter + 1)
      iter == 9 ->
        {mean, variance, prob_class} = classes_info.c9
        p = (gaussian(vector, mean, variance)
             |> Enum.reduce(&(&1 + &2))) + :math.log10(prob_class)
        prob_given_class(vector, classes_info,
                         Map.put_new(prob, :c9, p), iter + 1)
    end
  end

  def gaussian(vector, mean, variance) do
    vector
    |> Enum.zip(mean)
    |> Enum.zip(variance)
    |> Enum.map(fn {{e, m}, v} -> {e, m, v} end)
    |> Enum.map(fn {e, m, v} -> calculate(e, m, v) end)
    |> Enum.map(fn e ->
      if e == 0.0 do
        0.0
      else
        :math.log10(e)
      end
    end)
  end

  defp calculate(elem, mean, variance) do
    (1/:math.sqrt(2*:math.pi*variance)) *
    :math.exp(-0.5*(((elem - mean)*(elem - mean)) / variance))
  end
end
