defmodule Naive.CLI do

  @moduledoc """
  This module creates the interface between the user ant the command line.
  It handles the input and uses the Naive Bayes algorithm for the training.
  """

  @doc """
  Start the naive bayes algorithm with the desired parameters.
  """
  def main(argv) do
    argv
    |> parse_args
    |> process_args
    |> Naive.Learning.setup
  end

  @doc """
  `argv` can be -h or --help, which returns :help.

  Otherwise, it can be a data file (.csv) followed by the indices path 
  (.csv).

  Returns `:help`, a tuple of `{data_path, image_path}` or a tuple of
  `data_path`.
  """
  def parse_args(argv) do
    parsed_args = OptionParser.parse(argv, switches: [help: :boolean],
                                     aliases: [h: :help])
    case parsed_args do
      {[help: true], _, _} -> :help
      {_, [data_path, indices_path], _} -> {data_path, indices_path}
      _ -> :help
    end
  end

  def process_args(:help) do
    IO.puts """
    usage:  naive <data-path> <image-path>
    """
    System.halt(0)
  end

  def process_args({data_path, indices_path}) do
    indices = process_indices(indices_path)
    data = process_data(data_path)
    filter_data(data, indices, [], 0)
  end

  def filter_data(_data, [], acc_list, _), do: acc_list
  def filter_data([],  _ind, acc_list, _), do: acc_list
  def filter_data([hd | td], ind = [hi | ti], acc_list, acc_index) do
    if hi == acc_index do
      filter_data(td, ti, [hd | acc_list], acc_index + 1)
    else
      filter_data(td, ind, acc_list, acc_index + 1)
    end
  end
    
  def process_indices(indices_path) do
    indices_path
    |> Path.expand
    |> File.stream!
    |> Enum.map(&(String.replace(&1, ~r{\n}, "")))
    |> Enum.map(&(String.to_integer(&1)))
    |> Enum.sort
  end

  def process_data(data_path) do
    data_path
    |> Path.expand
    |> File.stream!
    |> Enum.map(&(String.replace(&1, ~r{\n}, "")))
  end
  
end
