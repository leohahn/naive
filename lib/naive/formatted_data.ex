defmodule Naive.FormattedData do

  @max_iter 10
  @base [c9: 0, c8: 0, c7: 0, c6: 0, c5: 0,
         c4: 0, c3: 0, c2: 0, c1: 0, c0: 0]
  @base_matrix [c9: [0,0,0,0,0,0,0,0,0,0],
                c8: [0,0,0,0,0,0,0,0,0,0],
                c7: [0,0,0,0,0,0,0,0,0,0],
                c6: [0,0,0,0,0,0,0,0,0,0],
                c5: [0,0,0,0,0,0,0,0,0,0],
                c4: [0,0,0,0,0,0,0,0,0,0],
                c3: [0,0,0,0,0,0,0,0,0,0],
                c2: [0,0,0,0,0,0,0,0,0,0],
                c1: [0,0,0,0,0,0,0,0,0,0],
                c0: [0,0,0,0,0,0,0,0,0,0]]
  
  
  def iteration_class_data(predictions) do
    precision = calculate_precision(predictions, [], 0)
    recall = calculate_recall(predictions, [], 0)
    f_measure  = calculate_f_measure(precision, recall, [], 0)
    accuracy = calculate_accuracy(predictions)
    confusion_matrix = calculate_confusion_matrix(predictions, [], 0)

    [precision, recall, f_measure, accuracy, confusion_matrix]
  end

  def calculate_confusion_matrix(predictions, matrix, @max_iter), do: matrix
  def calculate_confusion_matrix(predictions, matrix, class) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}
    
    p0 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c0 and real == class
    end)
    |> Enum.count
    p1 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c1 and real == class
    end)
    |> Enum.count
    p2 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c2 and real == class
    end)
    |> Enum.count
    p3 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c3 and real == class
    end)
    |> Enum.count
    p4 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c4 and real == class
    end)
    |> Enum.count
    p5 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c5 and real == class
    end)
    |> Enum.count
    p6 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c6 and real == class
    end)
    |> Enum.count
    p7 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c7 and real == class
    end)
    |> Enum.count
    p8 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c8 and real == class
    end)
    |> Enum.count
    p9 = predictions
    |> Enum.filter(fn {key, _, real} ->
      key == :c9 and real == class
    end)
    |> Enum.count

    # For each class, gives back each predicted 
    m = {iter[class], [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9]}
    result = [m | matrix]
    calculate_confusion_matrix(predictions, result, class + 1)
  end
  
  defp calculate_accuracy(precision) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}

    size = Enum.count(precision)

    correct = precision
    |> Enum.filter(fn {key, val, class} ->
      key == iter[class]
    end)  
    |> Enum.count

    correct / size
  end
  
  defp calculate_f_measure(_, _, f_m, @max_iter), do: f_m
  defp calculate_f_measure(precision, recall, f_m, class) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}

    [{_, class_prec}] = precision
    |> Enum.filter(fn {key, _} -> key == iter[class] end)

    [{_, class_recall}] = recall
    |> Enum.filter(fn {key, _} -> key == iter[class] end)

    class_f_m = (2 * class_recall * class_prec) / (class_recall + class_prec)
    result = [{iter[class], class_f_m} | f_m]
    calculate_f_measure(precision, recall, result, class + 1)
  end
  
  defp calculate_precision(predictions, precision, @max_iter), do: precision
  defp calculate_precision(predictions, precision, class) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}

    class_predictions = predictions
    |> Enum.filter(fn {pred, _, real} -> iter[class] == pred end)

    size = Enum.count(class_predictions)

    correct_predictions = class_predictions
    |> Enum.filter(fn {pred, _, real} -> class == real end)
    |> Enum.count
    
    result = [{iter[class], correct_predictions / size} | precision]
    calculate_precision(predictions, result, class + 1)
  end

  defp calculate_recall(predictions, recall, @max_iter), do: recall
  defp calculate_recall(predictions, recall, class) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}

    vp = predictions
    |> Enum.filter(fn {pred, _, real} ->
      pred == iter[class] and real == class
    end)
    |> Enum.count

    vp_fn = predictions
    |> Enum.filter(fn {pred, _, real} ->
      real == class
    end)
    |> Enum.count
    
    result = [{iter[class], vp / vp_fn} | recall]
    calculate_recall(predictions, result, class + 1)
  end

  def format_results(results) do
    insert_row
    insert_iter_info(results)

    results
    |> mean_of_iterations
    |> insert_mean_info
  end

  def insert_mean_info(mean) do
    IO.gets("Press Enter to final Statistics...")
    IO.puts("MEAN OF ALL ITERATIONS:")

    IO.puts("Precision: ")
    mean
    |> Enum.at(0)
    |> Enum.reverse
    |> Enum.each(fn {k, v} ->
      IO.puts(Atom.to_string(k) <> ": " <> Float.to_string(v,
                                                           decimals: 2))
    end)
    IO.puts("")
    IO.puts("Recall: ")
    mean
    |> Enum.at(1)
    |> Enum.reverse
    |> Enum.each(fn {k, v} ->
      IO.puts(Atom.to_string(k) <> ": " <> Float.to_string(v,
                                                           decimals: 2))
    end)
    IO.puts("")
    IO.puts("Measure F: ")
    mean
    |> Enum.at(2)
    |> Enum.reverse
    |> Enum.each(fn {k, v} ->
      IO.puts(Atom.to_string(k) <> ": " <> Float.to_string(v,
                                                           decimals: 2))
    end)
    IO.puts("")
    IO.puts("Accuracy: ")
    mean
    |> Enum.at(3)
    |> Float.to_string(decimals: 2)
    |> IO.puts
    IO.puts("")
    mean
    |> Enum.at(4)
    |> format_matrix
    
    
  end

  def mean_of_iterations(results) do    
    m_precision = mean_precision(@base, results, 0)
    m_recall    = mean_recall(@base, results, 0)
    m_measure_f = mean_measure_f(@base, results, 0)
    m_accuracy  = mean_accuracy(0, results, 0)        
    m_matrix    = mean_confusion_matrix(@base_matrix, results, 0)
    
    [m_precision, m_recall, m_measure_f, m_accuracy, m_matrix]
  end

  def mean_confusion_matrix(matrix, _, @max_iter) do
    matrix
    |> Enum.map fn {k, list} ->
      {k, list |> Enum.map(&(&1 / 10))}
    end
  end

  def mean_confusion_matrix(matrix, results, iter) do
    results[iter]
    |> Enum.at(4)
    |> Enum.zip(matrix)
    |> Enum.map(fn {{k, l}, {kr, lr}} ->
      {k, sum_lists(l, lr)} 
    end)
    |> mean_confusion_matrix(results, iter + 1)
  end

  defp sum_lists(list1, list2) do
    list1
    |> Enum.zip(list2)
    |> Enum.map(fn {e1, e2} -> e1 + e2 end)
  end
  
  def mean_recall(recall, _, @max_iter) do
    recall
    |> Enum.map fn {k, v} ->
      {k, v/10}
    end
  end

  def mean_recall(recall, results, iter) do
    results[iter]
    |> Enum.at(1)
    |> Enum.zip(recall)
    |> Enum.map(fn {{k, v}, {kr, vr}} -> {k, v + vr} end)
    |> mean_recall(results, iter + 1)
  end
  
  def mean_measure_f(m_f, _, @max_iter) do
    m_f
    |> Enum.map fn {k, v} ->
      {k, v/10}
    end
  end

  def mean_measure_f(m_f, results, iter) do
    results[iter]
    |> Enum.at(2)
    |> Enum.zip(m_f)
    |> Enum.map(fn {{k, v}, {kr, vr}} -> {k, v + vr} end)
    |> mean_measure_f(results, iter + 1)
  end

  def mean_accuracy(accuracy, _, @max_iter) do
    accuracy / 10
  end

  def mean_accuracy(accuracy, results, iter) do
    results[iter]
    |> Enum.at(3)
    |> (&(&1 + accuracy)).()
    |> mean_accuracy(results, iter + 1)
  end
  
  def mean_precision(precision, _, @max_iter) do
    precision
    |> Enum.map fn {k, v} ->
      {k, v/10}
    end
  end
  
  def mean_precision(precision, results, iter) do
    results[iter]
    |> Enum.at(0)
    |> Enum.zip(precision)
    |> Enum.map(fn {{k, v}, {kr, vr}} -> {k, v + vr} end)
    |> mean_precision(results, iter + 1)
  end
  
  defp insert_iter_info(results) do
    for it <- 0..9 do
      insert_row
      IO.gets("PRESS TO CONTINUE...")
      IO.puts("ITERATION " <> Integer.to_string(it + 1))
      insert_class_info(results[it], it)
    end
  end

  def insert_class_info(results, it) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}

    [precision, recall, m_f, accuracy, matrix] = results

    IO.puts("Accuracy: " <> Float.to_string(accuracy, decimals: 4))
    IO.puts("")
    for {val, key} <- iter do
      IO.puts("Class " <> Integer.to_string(val) <> ":")
      IO.puts("Precision: " <> Float.to_string(precision[key],
                                               decimals: 4))
      IO.puts("Recall   : " <> Float.to_string(recall[key],
                                            decimals: 4))
      IO.puts("F Measure: " <> Float.to_string(m_f[key],
                                               decimals: 4))
    end

    format_matrix(matrix)
  end

  def format_matrix(matrix) do
    add_header_row
    add_row(matrix, 0)
    add_row(matrix, 1)
    add_row(matrix, 2)
    add_row(matrix, 3)
    add_row(matrix, 4)
    add_row(matrix, 5)
    add_row(matrix, 6)
    add_row(matrix, 7)
    add_row(matrix, 8)
    add_row(matrix, 9)
  end

  def add_header_row do
    IO.puts ""
    IO.write """
       | 0    | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    |
    ---+------+------+------+------+------+------+------+------+------+------+
    """
  end

  def add_row(matrix, class) do
    iter = %{0 => :c0, 1 => :c1, 2 => :c2, 3 => :c3, 4 => :c4,
             5 => :c5, 6 => :c6, 7 => :c7, 8 => :c8, 9 => :c9}

    ls = matrix[iter[class]]
    c0 = Enum.at(ls, 0)
    c1 = Enum.at(ls, 1)
    c2 = Enum.at(ls, 2)
    c3 = Enum.at(ls, 3)
    c4 = Enum.at(ls, 4)
    c5 = Enum.at(ls, 5)
    c6 = Enum.at(ls, 6)
    c7 = Enum.at(ls, 7)
    c8 = Enum.at(ls, 8)
    c9 = Enum.at(ls, 9)

    iter = [c0, c1, c2, c3, c4, c5, c6, c7, c8, c9]

    IO.write("#{class}  |")
    for c <- iter do
      cond do
        c < 10 ->
          IO.write(" " <> num_to_string(c) <> "  |")     
        c < 100 ->
          IO.write(" " <> num_to_string(c) <> " |")
        c >= 100 ->
          IO.write(" " <> num_to_string(c) <> "|")
      end
    end
    IO.puts ""
    IO.write """
    ---+------+------+------+------+------+------+------+------+------+------+
    """
  end

  def num_to_string(num) do
    cond do
      is_float(num) -> Float.to_string(num, decimals: 1)
      is_integer(num) ->
        Float.to_string(num/1, decimals: 1)
    end
  end
  
  def add_table_row do
    IO.puts "-----------------------------------------------------------"
  end
  
  def insert_row do
    IO.puts "------------------------------------"
  end
  
end
